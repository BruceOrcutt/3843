//
// Created by borcutt on 9/12/18.
//

#ifndef INC_1_ASSIGN1_H
#define INC_1_ASSIGN1_H

#endif //INC_1_ASSIGN1_H


void         displayBits(unsigned int n);
unsigned int setKthBit(unsigned int n, int k);
int          isPowerOfTwo(unsigned int n);
int          getNoBits(unsigned int a, unsigned int b);
unsigned int computeXor(unsigned int n);