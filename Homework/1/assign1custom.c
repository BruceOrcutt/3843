#include <stdio.h>
#include "assign1.h"
#define LENGTH 32768


void displayBits(unsigned int n) {
    // DisplayBits Displays an unsigned integer in biths Print a space after every 8 bits
    int i;

    printf("%7d = ",n);

    for(i = 32;i > 0;i--) {
        if (n & (1<<(i-1)))
            printf("1");
        else
            printf("0");

        //n = n<<1;

        if (((i)%8) == 1) {
            printf(" ");
        }
    } // end for loop

    printf("\n");
} // end displayBits


unsigned int setKthBit(unsigned int n, int k) {
    // Sets the kth bit of n from the right to 1.

    unsigned int r = 0;

    r = n | (1<<(k-1));

    return r;
} //end setKthBit

int isPowerOfTwo(unsigned int n) {
    // Is this a power of 2?

    int             i;
    int             count = 0;
    unsigned int    num =   n;

    for(i = 32;i > 0;i--) {
        if (n & (1<<(i-1))) count++;  // found a 1
        if (count > 1) return 0;      // two 1s means not a power of 2
    }
    if (count == 0) return 0; // no 1s found
    return 1; // if we get here, it's only 1 1

} //end isPowerOfTwo

int getNoBits(unsigned int a, unsigned int b) {
    //get number of bits different

    unsigned int temp = a;
    int          i;
    int          count = 0;

    a ^= b;  // XOR, get bits that are only in one number, not both

    for(i = 32;i > 0;i--)
        if (a & (1<<(i-1))) count++;  // found a 1

    return count;
} //end getNoBits

unsigned int computeXor(unsigned int n) {
    // computers the XOR of all the numbers from 1 to n (including n).

    int          i;
    unsigned int xtotal = 0;

    for(i = 1;i <= n;i--)
        xtotal ^= i;

    return xtotal;

} //end computeXor
