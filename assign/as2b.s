	.file	"as2b.c"
	.text
.globl function1
	.type	function1, @function
function1:
	pushl	%ebp
	movl	%esp, %ebp
	movl	8(%ebp), %edx
	movl	12(%ebp), %eax
	cmpl	%edx, %eax
	jle	.L2
	movl	%edx, %eax
.L2:
	popl	%ebp
	ret
	.size	function1, .-function1
.globl function2
	.type	function2, @function
function2:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx
	subl	$8, %esp
	movl	8(%ebp), %ebx
	movl	%ebx, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	function1
	movl	%ebx, 4(%esp)
	movl	%eax, (%esp)
	call	function1
	addl	$8, %esp
	popl	%ebx
	popl	%ebp
	ret
	.size	function2, .-function2
.globl main
	.type	main, @function
main:
	leal	4(%esp), %ecx
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ecx
	subl	$8, %esp
	movl	$5, 4(%esp)
	movl	$6, (%esp)
	call	function2
	addl	$8, %esp
	popl	%ecx
	popl	%ebp
	leal	-4(%ecx), %esp
	ret
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.3.3-5ubuntu4) 4.3.3"
	.section	.note.GNU-stack,"",@progbits
