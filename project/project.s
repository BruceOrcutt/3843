	###########
	# Computer Organization Project
	# Professor: Tosun
	# Student: Orcutt
	# abc123: csz860
	##########
	.file	"project.c"
	.text
	.globl	power
	.type	power, @function
power:
	pushl	%ebp
	movl	%esp, %ebp

	

	# INSERT YOUR CODE HERE
	# USE REGISTERS FOR LOCAL VARIABLES
	pushl	%esi		# so we can use it, callee save, use as index
	pushl	%edi		# so we can use it, callee save, use as original value

	movl	8(%ebp),  %edi 	# get the value
	movl	12(%ebp), %esi	# get the index
	movl	$1,%eax		# initialiaze eax with 1

.looppow:
	cmpl	$0,%esi		# is index zero yet (count backwards)
	je	.exitpow	# if it is, we're done
	imull	%edi,%eax	# if not, mult value to result again
	subl	$1,%esi		# subtract one from index
	jmp	.looppow
	
.exitpow:
	popl	%edi		# restore value of edi to caller value
	popl	%esi 		# restore the esi to caller value
	leave
	ret
	.size	power, .-power
	.globl	fillarray
	.type	fillarray, @function

fillarray:
	pushl	%ebp
	movl	%esp, %ebp


	# INSERT YOUR CODE HERE
	# USE REGISTERS FOR LOCAL VARIABLES

        # %eax =  %edx = x %ecx = n %ebx = index  %esi = value %edi = A
	# save callee registers
	pushl	%ebx
	pushl   %esi
	pushl	%edi

	# initialize
	movl	8(%ebp),  %edx	 # x
	movl	12(%ebp),  %edi	 # A
	movl	16(%ebp), %ecx	 # n
	subl	$1,%ecx	       	 # n = n - 1	
	movl	$0,%ebx		 # index	       	
	movl	$0,%esi		 # value

.loopfa:
	# compare index to n-1
	cmpl	%ecx, %ebx
	je	.exitfa
 
	
	# Get ready to call (reverse order)
	push	%ebx		# put index
	push	%edx		# put value
	call power
	movl	%eax, (%edi,%ebx,4)
	

	# increment the counter at end of loop
	addl	$1, %ebx
	jmp	.loopfa 	

.exitfa:
	# restore called registers	
	popl	%edi
	popl	%esi
	popl	%ebx
	leave
	ret
	.size	fillarray, .-fillarray
	.globl	fillarray2
	.type	fillarray2, @function
fillarray2:
        pushl   %ebp
        movl    %esp, %ebp


        # INSERT YOUR CODE HERE
        # USE REGISTERS FOR LOCAL VARIABLES

        # %eax =  %edx = x %ecx = n %ebx = index  %esi = value %edi = A
        # save callee registers
        pushl   %ebx
        pushl   %esi
        pushl   %edi

        # initialize
        movl    8(%ebp),  %edx   # x
        movl    12(%ebp),  %edi  # A
        movl    16(%ebp), %ecx   # n
        subl    $1,%ecx          # n = n - 1
        movl    $0,%ebx          # index
        movl    $1,%esi          # value

.loopfa2:
        # compare index to n-1
        cmpl    %ecx, %ebx
        je      .exitfa2

	# special case, power 0
	cmpl	$0,%ebx
	je	.fa2store	# if 0, go store the 1 instead of multiplying 
	
        # compute power 
	imull	%edx, %esi

.fa2store:
        movl    %esi, (%edi,%ebx,4)

        # increment the counter at end of loop
        addl    $1, %ebx
        jmp     .loopfa2

.exitfa2:
        # restore called registers
        popl    %edi
        popl    %esi
        popl    %ebx
        leave
        ret
	


ret
	.size	fillarray2, .-fillarray2
	.globl	compare
	.type	compare, @function
compare:
	pushl	%ebp
	movl	%esp, %ebp

	# INSERT YOUR CODE HERE
	# USE REGISTERS FOR LOCAL VARIABLES
	
	# init
        pushl   %ebx		# A
        pushl   %esi		# B
        pushl   %edi		# n
	movl	$1, %eax	# return result
	movl	$0, %ecx	# index
	movl	$0, %edx	# temp compare

	# load params
	movl	8(%ebp),  %ebx	# get A
	movl	12(%ebp), %esi	# get B
	movl	16(%ebp), %edi 	# get n

	subl	$1,%edi		# go until n-1
.loopcompare:
	cmpl	%edi,%ecx	# have we finished the last index?
	je	.exitcompare
	
	movl	(%ebx,%ecx,4), %edx
	cmpl	(%esi,%ecx,4), %edx
	jne	.exitcomparefalse
	addl	$1,%ecx
	jmp	.loopcompare


.exitcomparefalse: 
	# come here if we failed a check
	movl	$0,%eax
.exitcompare:
        popl   	%edi
        popl   	%esi
        popl   	%ebx
	leave
	ret
	.size	compare, .-compare
	.section	.rodata
.LC0:
	.string	"fillarray(2,a,10) correct"
.LC1:
	.string	"fillarray(2,a,10) incorrect"
.LC2:
	.string	"fillarray2(2,b,10) correct"
.LC3:
	.string	"fillarray2(2,b,10) incorrect"
.LC4:
	.string	"fillarray(3,a,10) correct"
.LC5:
	.string	"fillarray(3,a,10) incorrect"
.LC6:
	.string	"fillarray2(3,b,10) correct"
.LC7:
	.string	"fillarray2(3,b,10) incorrect"
	.text
	.globl	main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$192, %esp
	movl	$2, 28(%esp)
	movl	$1, 112(%esp)
	movl	$2, 116(%esp)
	movl	$4, 120(%esp)
	movl	$8, 124(%esp)
	movl	$16, 128(%esp)
	movl	$32, 132(%esp)
	movl	$64, 136(%esp)
	movl	$128, 140(%esp)
	movl	$256, 144(%esp)
	movl	$512, 148(%esp)
	movl	$1, 152(%esp)
	movl	$3, 156(%esp)
	movl	$9, 160(%esp)
	movl	$27, 164(%esp)
	movl	$81, 168(%esp)
	movl	$243, 172(%esp)
	movl	$729, 176(%esp)
	movl	$2187, 180(%esp)
	movl	$6561, 184(%esp)
	movl	$19683, 188(%esp)
	movl	$10, 8(%esp)
	leal	32(%esp), %eax
	movl	%eax, 4(%esp)
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	fillarray
	movl	$10, 8(%esp)
	leal	112(%esp), %eax
	movl	%eax, 4(%esp)
	leal	32(%esp), %eax
	movl	%eax, (%esp)
	call	compare
	cmpl	$1, %eax
	jne	.L17
	movl	$.LC0, (%esp)
	call	puts
	jmp	.L18
.L17:
	movl	$.LC1, (%esp)
	call	puts
.L18:
	movl	$10, 8(%esp)
	leal	72(%esp), %eax
	movl	%eax, 4(%esp)
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	fillarray2
	movl	$10, 8(%esp)
	leal	112(%esp), %eax
	movl	%eax, 4(%esp)
	leal	72(%esp), %eax
	movl	%eax, (%esp)
	call	compare
	cmpl	$1, %eax
	jne	.L19
	movl	$.LC2, (%esp)
	call	puts
	jmp	.L20
.L19:
	movl	$.LC3, (%esp)
	call	puts
.L20:
	movl	$3, 28(%esp)
	movl	$10, 8(%esp)
	leal	32(%esp), %eax
	movl	%eax, 4(%esp)
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	fillarray
	movl	$10, 8(%esp)
	leal	152(%esp), %eax
	movl	%eax, 4(%esp)
	leal	32(%esp), %eax
	movl	%eax, (%esp)
	call	compare
	cmpl	$1, %eax
	jne	.L21
	movl	$.LC4, (%esp)
	call	puts
	jmp	.L22
.L21:
	movl	$.LC5, (%esp)
	call	puts
.L22:
	movl	$10, 8(%esp)
	leal	72(%esp), %eax
	movl	%eax, 4(%esp)
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	fillarray2
	movl	$10, 8(%esp)
	leal	152(%esp), %eax
	movl	%eax, 4(%esp)
	leal	72(%esp), %eax
	movl	%eax, (%esp)
	call	compare
	cmpl	$1, %eax
	jne	.L23
	movl	$.LC6, (%esp)
	call	puts
	jmp	.L25
.L23:
	movl	$.LC7, (%esp)
	call	puts
.L25:
	leave
	ret
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04.4) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
